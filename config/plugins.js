module.exports = () => ({
  "strapi-plugin-populate-deep": {
    config: {
      defaultDepth: 7, // Default is 5
    },
  },
});
